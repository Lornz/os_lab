#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int main(){
/*
server: call shmget that creates the the shared memory, return shmid;
you pass the key used to access to the shared memory 
- IPC_CREATE flag for creating the segment
- set permission eg 0666

use shmid to pass it to shmat
shmat(smget(...))
	- shmid
	- NULL: no preference for starting point of shared memory
		the OS decides it
	- 0 : non so
shmat return a pointer to the shared memory

Shared memory can result in  a race condition
Here the synch is done with server waiting for the first character to become '*'
*/

/*
CLIENT
use the same key to shmget (same permission)
use shmat that return the pointer
the client read from shared memory and write to stdout
*/
}
