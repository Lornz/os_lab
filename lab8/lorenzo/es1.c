#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>

void *printfunction(void* idpt){
	unsigned char id= *( (char*) idpt);
	switch(id){
		case 1:
			printf("First thread\n");
			sleep(5);
			printf("First thread terminates\n");
			break;
		case 2: 
			printf("Second thread\n");
			sleep(5);
			printf("Second thread terminates\n");
			break;
		default:
			printf("wrong id\n");
	}
	//return;
	pthread_exit(NULL);

}
int main(){
	//thread id
	pthread_t thread1, thread2;
	//argument
	unsigned int arg1,arg2;	
	//create thread
	int rc;
	arg1=1;
	arg2=2;
	//DO NOT change the variable where arguments are stored! 
	rc= pthread_create(&thread1, NULL, printfunction, (void *) &arg1);
	rc+= pthread_create(&thread2, NULL, printfunction, (void *) &arg2);
	//wait for join
	pthread_join(thread1, NULL);
	printf("Joined with first thread\n");
	pthread_join(thread2, NULL);
	printf("Joined with second thread\n");
	
	printf("Father exited\n");
	return 0;
}
