#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

char match_str[20];
char in_path[]="./input3.txt";

unsigned long int in_size;
int thread_num;

void *thread_read(void* id){
	FILE *infile= fopen(in_path, "r");
	int myid = ( int) id;
	int size=in_size/thread_num+1;
	//round up and space for \0
	char *buff=(char *)malloc(size+ 1);

	fseek(infile, myid*(in_size/thread_num + 1),SEEK_SET);
	int temp=fgets(buff, size, infile);
	
	int m=0;
	int end_pos;
	int i=0;
	while( m!=0 || i<strlen(buff)){
	
		if(buff[i]==match_str[m]){
			m++;
			if(m==strlen(match_str)-1){
				printf("found for %d\n",i);
				m=0;
			}
		
		} else {
			if(m!=0){
				//check for new sequence
				if(buff[i]=match_str[0]){
					m=1;
				} else
					m=0;
			} 
		}
	i++;
	}
	pthread_exit(0);
}
int main(int argc, char *argv[]){
	if(argc <2){
		printf("Add the number of processes\n");
		return 1;
	}
	thread_num=atoi(argv[1]);
	//instantiate a vector of id
	pthread_t *pthr_id = (pthread_t *) malloc(sizeof(pthread_t)*thread_num);
	
	//open file
	char match_path[]="./match3.txt";

	FILE *fmatch = fopen(match_path, "r");
	//read the match string
	fgets(match_str, sizeof(match_str), fmatch);
	fclose(fmatch);
		
	//find file size
	FILE *finput =fopen(in_path, "r"); //use global variable
	fseek(finput, 0, SEEK_END);
	in_size=ftell(finput); //global variable
	fclose(finput);
		
	for(int i=0;i<thread_num;i++){
		pthread_create(pthr_id+i, NULL,thread_read,(void*)i );
	}
	pthread_join(pthr_id[0],NULL);
}
