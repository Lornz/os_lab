#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads

#define MAX_GENOME_SIZE 50000
#define MAX_SEQUENCE_SIZE 7
#define MAX_MSG_SIZE 20

//Shared variable among threads
char genome[MAX_GENOME_SIZE];
char sequence[MAX_SEQUENCE_SIZE];
int  divider=0;			//this is to count the number of created threads and equally split the genome dimension to better start searching for the sequence
int global_index=0;
char indexes[MAX_GENOME_SIZE/MAX_SEQUENCE_SIZE][MAX_MSG_SIZE];

//This is the procedure the thread will execute
void *Scanning (void * arg)
{
	long * tid;
	tid = (long *) arg;
	int genome_size=0;
	int starting_index=0;
	int max_scan,max_scan_index=0;
	char * f;
	char temp_sequence[7];	
	int j,k,h=0;	
	int temp_st, temp_end =0;
	int bho;

	for(f=&genome[0]; *f; f++){                             // f points successively to genome[0], genome[1], ... until a '\0' is observed.
                genome_size += 1;
        }
	
	genome_size = genome_size-2;		//it seems after the above for the last 2 chars are not a letter (one is \n, the other \0)
	//if(*tid==0) printf("genome_size = %d \t counter = %d \n", genome_size, divider);
	
	starting_index = (genome_size/divider)*(* tid);		//This will return all the indexes positions to start from for the various threads
	max_scan= (genome_size/divider) + 7;			//This is to avoid useless overlapping while searching for sequence; +7 is the max allowed overlap
	max_scan_index = starting_index + max_scan;	
	if(max_scan_index > genome_size) max_scan_index = genome_size;
	printf("Thread %ld has starting index %d, max scan index %d\n", *tid, starting_index, max_scan_index);
	
	while(starting_index+k+7<max_scan_index){
	
		for(k=0;k<max_scan_index-7-starting_index;k++){
			h=0;
			for(j=starting_index+k;j<starting_index+7+k;j++){
	       	         	temp_sequence[h]=genome[j];
				h=h+1;
			}
			printf("This is temp_sequence %s, while sequence is %s\n", temp_sequence,sequence);
			if ( strncmp(sequence,temp_sequence,7) == 0){		//This was necessary to have just 7 chars checked and not the \n in sequence
       		        	temp_st = starting_index+k;
				temp_end = starting_index+k+7;
				bho = snprintf(indexes[global_index], MAX_MSG_SIZE, "%d - %d",temp_st,temp_end);
				global_index +=1; 
				//printf("\nFound a match between %d and %d\n", temp_st, temp_end);        
       	       	}
 
        }
}

	//if( *tid == 0) starting_index =0;
	//else starting_index =(genome_size/(* tid))*(* tid);
	//printf("Thread %ld starting index is: %f\n", *tid, starting_index);	
	/*
	int running = 1;
	while(running){
		printf("\nEnter a string > \t");
		fgets(command, MAX_SIZE, stdin);		
		if ( strcmp(command,"house\n")==0 || strcmp(command,"casa\n")==0) {
			printf("%s detected!\n", command);
		}	
		if ( strcmp(command,"exit\n")==0) running =0;
	}
	*/
	pthread_exit(NULL);
}


int main(int argc, char *argv[]){
	int rc;
	char c;
	FILE * fp1,* fp2;		//create pointer fp1 to genome.txt file and fp2 to sequence.txt
	char * f;			//f is a char pointer to check each char in genome string is C G A T or it will return an error
	int P=0;			//This will be the user defined number of threads to use
	int t=0;	

	fp1=fopen("genome.txt","r");	//open genome.txt in reading mode
	
	while(fgets(genome, MAX_GENOME_SIZE, fp1)!=NULL);	//fgets used to read the entire string in genome.txt and store it into genome, last char will be '\0'
	
	//The following is just to check if the genome is correct before proceding as well as to get the actual genome size to later handle correctly the indexes
	for(f=&genome[0]; *f; f++){ 				// f points successively to genome[0], genome[1], ... until a '\0' is observed.
		c= *f;
		if( c!='C' && c!='A' && c!='G' && c!='T' && c!='\0' && c!='\n'){
			printf("Error in genome!\n");
			exit(-1);
		}		
	}

	fp2=fopen("sequence.txt", "r");	//open sequence.txt in read mode

	while(fgets(sequence, 9, fp2)!= NULL);			//fgets used to read the entire string in sequence.txt and store it into sequence, last char will be \0 so it must be eliminated

	//Beginning of the actual program

	printf("Hello! Genome and sequence have been loaded! How many threads would you like to use to search of DNA sequence?\n");
	scanf("%d",&P);
	divider=P;
	pthread_t threads[P];
	long t_numb[P];			//like with es1 to avoid sync probs
	
	//creating the threads
	for(t=0; t<P; t++){
		t_numb[t]=t;
		printf("I'm Main creating thread number %d\n", t);
		rc = pthread_create(&threads[t], NULL, Scanning, (void *) &t_numb[t]); //last arg passed as it will be used for moving the index in the research
		if (rc) {
			printf("ERROR in return code from pthread_create: %d\n", rc);
			exit(-1);
		}			
	}
	//pthread_join(tid,NULL);
	for(t=0; t<P; t++){
		pthread_join(threads[t],NULL);
	}
	printf("You found the following sequences:\n");
	for(int l=0; l<global_index; l++){
		printf("%s\n",indexes[l]);
	}
	printf("Terminating the program\n");	
	return 0;
}
