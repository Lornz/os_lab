#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads


//#define MAX_TEST_NUMBERS 500

//Global variable definition

char no_divisor=0;		//flag set to 1 if a certain thread did not find a divisor
char found_divisor=0;		//flag to set to 1 whether any thread finds a divisor

//Could be wise to use a structure passed as thread argument, with the structure containing the number N, the number of threads (thread id) and the divisor interval to test for each thread

struct thread_data 
{
	int thread_id;
	int test_number;
	int thread_num;
};

//This is the procedure the thread will execute
void *Scanning (void * arg)
{
	int tid,N,P;
	struct thread_data *my_data;
	my_data=(struct thread_data *) arg;
	tid = my_data->thread_id;
	N = my_data->test_number;
	P = my_data->thread_num;	
	int i=0;
	int num_to_test = (N/2-2)/P;				//This is the range to test
	if( (N/2-2)%P!=0 ) num_to_test=num_to_test +1; 		//If the mod is not zero then add +1 so we round to next integer
	
	//printf("This is num_to_test: %d\n", num_to_test);
	//printf("This is the max of the first loop %d\n", num_to_test*(tid +1) );
	//This is true only for thread with id =0
	if(tid == 0){
		if(N%2==0 && N>2){
			//if the number is even, just discard it			
			found_divisor=1;
			pthread_exit(NULL);
		}		
		for(i=2; i <= num_to_test*(tid +1); i++){
			if (N%i ==0) { 
				//if you find a divisor for N, N is not prime so just exit
				found_divisor = 1;
				//printf("Tid %d exit\n",tid);
				pthread_exit(NULL);
			}
		}
		
	}
	else if(tid > 0) {
		if(N%2==0 && N>2){ 
			//if the number is even just discard it
			found_divisor=1;
			pthread_exit(NULL);
		}
		printf("Thread tid: %d will check from %d to %d\n",tid, num_to_test*tid+1, num_to_test*(tid+1));
		for(i=num_to_test*tid +1 ; i<=num_to_test*(tid +1); i++){
			if (N%i ==0) { 
				//if you find a divisor for N, N is not prime so just exit
				found_divisor=1;
				//printf("Tid %d exit\n",tid);
				pthread_exit(NULL);
			}		
		}
	}
	//This part is reached only if N is actually a prime number	
	no_divisor=1;
	pthread_exit(NULL);
}


int main(int argc, char *argv[]){
	int rc;
	int P=0;			//This will be the user defined number of threads to use
	int t=0;	
	int N=0;
	int * f;			//Pointer needed to pass the interval of divisors to test

	//Beginning of the actual program

	printf("Which number N would you like to check wheter it's prime or not?\n");
	scanf("%d",&N);
	printf("Enter the number of threads you would like to use for checking\n");
	scanf("%d",&P);
	
	pthread_t threads[P];
	struct thread_data thread_data_array[P];	//like with es1 to avoid sync probs
	

	//creating the threads
	for(t=0; t<P; t++){
		thread_data_array[t].thread_id=t;
		thread_data_array[t].test_number=N;
		thread_data_array[t].thread_num=P;
		//printf("I'm Main creating thread number %d\n", t);
		rc = pthread_create(&threads[t], NULL, Scanning, (void *) &thread_data_array[t]); //last arg passed as it will be used for moving the index in the research
		if (rc) {
			printf("ERROR in return code from pthread_create: %d\n", rc);
			exit(-1);
		}			
	}
	//pthread_join(tid,NULL);
	for(t=0; t<P; t++){
		pthread_join(threads[t],NULL);
	}

	if(no_divisor == 1 && found_divisor ==0){
		printf("%d is a prime number!\n",N);
	}
	else {
		printf("%d is not a prime number!\n",N);
	}

	printf("Terminating the program\n");	
	return 0;
}
