#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads

#define NUM_THREADS 2

//This is what the thread will execute
void *PrintMessages (void * threadid)
{
	long * tid;
	tid = (long *) threadid;
	printf("Thread number %ld has been created. \n", *tid);
	printf("Thread number %ld is processing...\n", *tid);
	sleep(5);
	printf("Thread number %ld terminated.\n", *tid);
	pthread_exit(NULL);
}

int main(int argc, char *argv[]){
	pthread_t threads[NUM_THREADS];
	int rc,t;
	long t_numb[NUM_THREADS];
	
	for(t=0;t<NUM_THREADS;t++){
		t_numb[t]=t;
		printf("I'm main creating thread number %d\n", t);
		//Remember the synthax! usually 2nd is null, 3rd is the procedure name and 4th is the procedure argument which is, usually, a void pointer, that's why there is the casting of &t!
		rc = pthread_create(&threads[t], NULL, PrintMessages, (void *) &t_numb[t]);
		if (rc) {
			printf("ERROR in return code from pthread_create: %d\n", rc);
			exit(-1);
		}
		sleep(1);
	}
	for(t=0;t<NUM_THREADS;t++){
		pthread_join(threads[t],NULL);
	}
	return 0;
}
