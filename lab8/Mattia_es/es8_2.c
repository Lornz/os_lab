#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads

#define MAX_SIZE 20

//Shared variable among threads
char command[MAX_SIZE];

//This is the procedure the thread will execute
void *Scanning (void * vargp)
{
	int running = 1;
	while(running){
		printf("\nEnter a string > \t");
		fgets(command, MAX_SIZE, stdin);		
		if ( strcmp(command,"house\n")==0 || strcmp(command,"casa\n")==0) {
			printf("%s detected!\n", command);
		}	
		if ( strcmp(command,"exit\n")==0) running =0;
	}
	pthread_exit(NULL);
}

int main(int argc, char *argv[]){
	pthread_t tid;
	int rc;

	printf("Program started \n");
	rc = pthread_create(&tid, NULL, Scanning, NULL); //last arg is null cause there is no need to pass anything to the thread
	if (rc) {
		printf("ERROR in return code from pthread_create: %d\n", rc);
		exit(-1);
	}

	pthread_join(tid,NULL);
	printf("Terminating the program\n");	
	return 0;
}
