#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads
#include <semaphore.h>		//library necessary to use semaphores related functions
#include <math.h>		//library to allow usage of particular math functins eg pow(), to compile use gcc filename.c -lm -o filename.bin

#define READ 0
#define WRITE 1

int main(){
	pid_t pid[2];		//to create two child processes
	FILE *fp;		//to be able to open a file called sensors.txt and save data in it (open in write mode)
	int fd1[2],fd2[2];	//create two unnamed pipes to let the parent communicate with its children
	int count1,count2;	//if count reaches 2 then two times -1 has been received so stop the program	
	int rand_num;
	int read1,read2;	//where the value read from pipe will be stored
	int rb1,rb2;		//bytes read;
	int read1_b,read2_b;

	//create the pipes
	pipe(fd1);
	pipe(fd2);

	//open file in writing mode
	fp=fopen("sensors.txt","w");		//create a new file for writing on it (if it already exists, erease its content)

	//create child processes
	pid[0]=fork();
	pid[1]=fork();

	//NOTE: remember it will be the OS to close the pipe ends whenever children are terminated by the parent process

	//child 1: SENSOR PROCESS
	if(pid[0]==0){
		close(fd1[READ]);
		srand(getpid());		//change seed for random number generation
		while(1){
			usleep((rand()%1501+500)*1000);		//sleep for microseconds, times 1000 becomes milliseconds etc..here it waits from 0.5 to 2 secs
			rand_num=rand()%7-3;			//generate rand numbers between -3 and 3 (rand()%(max-min+1)+min)
			write(fd1[WRITE],&rand_num,sizeof(int));				
		}
	}
	//child 2: SENSOR PROCESS
	else if(pid[1]==0){
		close(fd2[READ]);
		srand(getpid());
		while(1){
			usleep((rand()%1501 +500)*1000);
			rand_num=rand()%7-3;
			write(fd2[WRITE],&rand_num,sizeof(int));
		}
	}
	//parent: WRITER PROCESS
	else{
		close(fd1[WRITE]);
		close(fd2[WRITE]);
		signal(SIGKILL,SIG_DFL);		//set default handler for sigkill
		while(1){
			//read from first pipe
			rb1=read(fd1[READ],&read1,sizeof(int));
			if(rb1<0) perror("Error reading pipe1\n");
			//read from second pipe
			rb2=read(fd2[READ],&read2,sizeof(int));
			if(rb2<0) perror("Error reading pipe2\n");
			
			//for pipe1
			if(read1==-1 && read1_b==-1){
				fseek(fp,-20, SEEK_END);		//move pointer to delete last 20 chars
				ftruncate(fileno(fp),ftell(fp));	//POSIX functions	
				break;					//get out
			}
			else{
				printf("pipe1: %d\n",read1);
				read1_b=read1;
				fprintf(fp,"pipe1: %d\n",read1);
			}
			
			//for pipe2
			if(read2==-1 && read2_b==-1){
				fseek(fp,-20, SEEK_END);		//move pointer to delete last 20 chars
				ftruncate(fileno(fp),ftell(fp));	//POSIX functions	
				break;					//get out
			}
			else{
				printf("pipe2: %d\n", read2);
				read2_b=read2;
				fprintf(fp,"pipe2: %d\n",read2);
			}
			

		}

		close(fd1[READ]);
		close(fd2[READ]);
	
		//two -1 were found so kill children
		kill(pid[0],SIGKILL);
		kill(pid[1],SIGKILL);

		//grieve children
		waitpid(pid[0],NULL,0);
		waitpid(pid[1],NULL,0);
		
		//Finally terminate the program

		printf("Terminating the program \n");
		
		return 0;
	}
}
