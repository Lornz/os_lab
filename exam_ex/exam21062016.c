#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads
#include <semaphore.h>		//library necessary to use semaphores related functions
#include <math.h>		//library to allow usage of particular math functins eg pow(), to compile use gcc filename.c -lm -o filename.bin

#define READ 0
#define WRITE 1

#define SHMSZ 2*sizeof(float)
#define BUFSIZE 10

char flag=0;
char ready=0;

void getout(){
	printf("\nSIGNAL: setting flag to 1 to let children terminate\n");
	flag=1;
}

void readytoread(){
	printf("\nSIGNAL: allowing a children to read\n");
	ready=1;
}

int main(){
	pid_t pid[2];
	int fd1[2],fd2[2];
	char input_values[BUFSIZE];
	int read_value,rb;
	int count,sum;
	int shmid;		//shared memory identifier
	float *shm;		//address to shmemory
	float average1, average2;
	
	//shared memory creation
	if((shmid=shmget(IPC_PRIVATE,SHMSZ,IPC_CREAT|0666))<0){
		perror("Error in shared memory creation\n");
	}

	//shared memory attach	
	if((shm=(float *) shmat(shmid,NULL,0)) == (float *) -1){

		perror("Error in shared memory attach\n");
	}

	//create pipes
	pipe(fd1);
	pipe(fd2);

	//create children processes
	for(int i=0; i<2; i++){
		pid[i]=fork();
		if(pid[i]<0){
			perror("Error in process creation\n");
		}
		if(pid[i]==0){
			break;
		}
	}


	//child 1
	if(pid[0]==0){
		close(fd1[WRITE]);
		signal(SIGUSR1,getout);
		signal(SIGUSR2,readytoread);
		count=0;
		sum=0;
		while(flag!=1){
			if(ready==1){
				rb=read(fd1[READ],&read_value,sizeof(int));
				if(rb<0) perror("Error during pipe read\n");
				printf("\t\t\t\t\t\tCHILD1: value read is %d\n",read_value);
				sum +=read_value;
				count++;
				printf("\t\t\t\t\t\tCHILD1: the current value of sum is %d and of count is %d\n",sum,count);
				ready=0;
			}
		}
		average1=((float)sum)/((float)count);
		*shm=average1;
		exit(0);				
	}
	//child 2
	else if(pid[1]==0){
		close(fd2[WRITE]);
		signal(SIGUSR1,getout);
		signal(SIGUSR2,readytoread);
		count=0;
		sum=0;
		shm++;		//so it can write in the other memory location
		while(flag!=1){
			if(ready==1){
				rb=read(fd2[READ],&read_value,sizeof(int));
				if(rb<0) perror("Error during pipe read\n");
				printf("\t\t\t\t\t\t\t\t\t\t\t\tCHILD2: value read is %d\n",read_value);
				sum +=read_value;
				count++;
				printf("\t\t\t\t\t\t\t\t\t\t\t\tCHILD2:the current value of sum is %d and of count is %d\n",sum,count);
				ready=0;
			}	
		}
		average2=((float)sum)/((float) count);
		*shm=average2;
		exit(0);				
	}
	//parent
	else{
		//close pipe read ports
		close(fd1[READ]);
		close(fd2[READ]);
		while(1){
			printf("PARENT:Enter a value to send to a children process:\n");
			fgets(input_values,BUFSIZE,stdin);
			printf("PARENT: Just read %s\n",input_values);
			read_value=atoi(input_values);
	
			//if read value is even...
			if(read_value%2==0 && read_value > 0){
				write(fd1[WRITE],&read_value,sizeof(int));
				kill(pid[0],SIGUSR2);
			}
			//if read value is odd...
			else if(read_value%2==1 && read_value >0){
				write(fd2[WRITE],&read_value,sizeof(int));
				kill(pid[1],SIGUSR2);
			}
			else if(read_value < 0){
				printf("PARENT: sending signals to children processes\n");
				kill(pid[0],SIGUSR1);
				kill(pid[1],SIGUSR1);
				break;
			}

		}	
	}
	
	printf("PARENT: terminating children processes...\n");
	
	waitpid(pid[0],NULL,0);
	waitpid(pid[1],NULL,0);

	printf("PARENT: children processes correctly terminated\n");

	//read averages from shared memory
	average1= *shm;
	shm++;
	average2= *shm;

	printf("The value passed from children processes are: \nCHILD1: %f\nCHILD2: %f\n",average1,average2);

	shmdt(shm);
	shmctl(shmid, IPC_RMID,0);

	printf("Terminating the program\n");
	
	return 0;
}

