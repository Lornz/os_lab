#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads
#include <semaphore.h>		//library necessary to use semaphores related functions
#include <math.h>		//library to allow usage of particular math functins eg pow(), to compile use gcc filename.c -lm -o filename.bin


int main()
{
	pid_t pid1,pid2;
	pid1 = fork(); /* call #1 */
	pid2 = fork(); /* call #2 */
	if (pid1 != pid2) fork(); /* call #3 */
	sleep(10);
	return 0;
}


/*int main(int argc, char *argv[]) {
	int x;
	pid_t pid;
	int status;
	x = 13;
	printf("x is now %d\n",x);
	pid = fork();
	if (pid == 0) {
		x = x+4;
		printf(" Add 4, and x = %d\n", x);
	exit(x);
	}
	else
	{
		x = x*4;
		printf(" Multiply by 4, and x = %d\n", x);
		wait(&status);
	}
	printf("In the end, x = %d\n", x);
	return 0;
}
*/
/*int main(int argc, char ** argv) 
	{
	int i;

	for (i=0; i < 3; i++) 
	{
		execl ("/bin/echo", "echo", "hello", NULL);
		printf("I’m out of exec \n");
	}
	return 0;
}

*/
/*int main( int argc, char *argv[], char *env[] )
	{   
  	 if(fork() == 0 )
  	 {
		printf("P1 (pid=%d): \n", getpid());
		sleep(5);
		kill(getppid(), SIGINT);
		printf("P1 (pid=%d): \n", getpid());
		exit(0);  
		printf("P1 (pid=%d): \n", getpid()); 
   	}
   	else    
   	{  
		printf("P2 (pid=%d): \n", getpid());
            	for(;;) ;
		printf("P2 (pid=%d): \n", getpid());
        }
	        printf("P2 (pid=%d): \n", getpid());
   	return 0;
}
*/
