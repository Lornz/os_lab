#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads
#include <semaphore.h>		//library necessary to use semaphores related functions
#include <math.h>		//library to allow usage of particular math functins eg pow(), to compile use gcc filename.c -lm -o filename.bin

#define READ 0
#define WRITE 1

char flag=0;		//global variable but will work only for child

void update(){
	printf("\nSIGNAL: received!\n");
	flag=1;
}

float average(int a, int b, int c){
	return ((a+b+c)/3);
}

int main(){
	pid_t pid;
	FILE *fp;
	int fd[2];
	int count;
	int rand_value;
	int rb;
	int read_nums[3];
	float avg, avg_b=0;	//_b stands for before


	//crete pipe
	pipe(fd);

	//create child process
	pid=fork();
	
	//child
	if(pid ==0){
		close(fd[READ]);				//close read end of pipe
		srand(getpid());
		signal(SIGUSR1,update);
		while(1){
			rand_value=rand()%90+10;				//rand number from 10 to 99 so we're sure they are always numbers with 2 chars
			write(fd[WRITE],&rand_value,sizeof(int));	//write in the pipe
			//printf("Child: flag is %c\n",flag);
			if (flag==1){
				//printf("Child entered cause flag was 1 body\n");
				sleep(3);
				flag=0;
				char *line=NULL;
				size_t len=0;
				char read_line;

				fp=fopen("sensors.txt","r");
				
				//move to last three lines
				fseek(fp,-9,SEEK_END);		//-9 because every read number in a line has 2 chars + the '\n' char
				
				
				for(int i=0; i<3; i++){
					read_line=getline(&line,&len, fp);
					read_nums[i]=atoi(line);		//read 3 lines, for each line up to 10 chars are read and convert string to int
					printf("\t\t\t\t\t\tChild: read_nums %d: %d\n",i,read_nums[i]);
				}
				if(line) free(line);
				avg=average(read_nums[0],read_nums[1],read_nums[2]);	//compute the average
				printf("\t\t\t\t\t\tChild: computed average: %f\n",avg);
				printf("\t\t\t\t\t\tChild: previous average: %f\n",avg_b);
				if(avg_b==0) {
					avg_b=avg;		//only the first time, just to have a starting point otherwise it would exit immediately
				}
				if(avg>2*avg_b){
					printf("Child: an error occurred: not good with ma brain bro! Gotta kill my parents MWAHAHHAAHHAA!\n");
					kill(getppid(),SIGINT);		//kill parent to close the whole program
				}
				avg_b=avg;
				fclose(fp);
				//printf("Child exiting cause flag is now 0\n");								
			}
			sleep(1);
		}		
	}
	//parent
	else{
		close(fd[WRITE]);
		count=0;
		char flag1=1;
		while(1){
			if(flag1==1){
				fp=fopen("sensors.txt","a");
				flag1=0;
			}
			rb=read(fd[READ],&rand_value,sizeof(int));		//read random value coming from child 
			if(rb<0) perror("Error While reading pipe\n");	
			usleep(10000);
			printf("\t\t\t\t\t\t\t\t\t\t\t\tParent:pipe read value: %d\n",rand_value);
			fprintf(fp,"%d\n",rand_value);			//print read value into sensors.txt		
			count ++;					//even if global variable, count is different between child and parent!
			//printf("Parent: count is %d\n",count);
			if(count==3){
				flag1=1;
				count=0;
				//printf("\t\t\t\t\t\t\t\t\tParent: count is %d\n",count);
				kill(pid, SIGUSR1);				//send signal to rise the flag
				fclose(fp);
				sleep(3);					//wait for the children to read...this could be done with semaphores?
			}
		}
	} 	
		
	return 0;
}

