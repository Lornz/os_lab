#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads
#include <semaphore.h>		//library necessary to use semaphores related functions
#include <math.h>		//library to allow usage of particular math functins eg pow(), to compile use gcc filename.c -lm -o filename.bin

#define SHMSIZ	4		//must fit 4 chars r/t/n r/t/n q q

void mixcards(char *card){
	srand(getpid());	//change random seed
	clock_t start,end;
	float timeout;
	char temp;
	char * position1=card;
	char * position2=card;
	
	start=clock();
	while(timeout<=5){
		
		//for some strange reason sometimes positions are set to 0 so the while covers for them
		position1=card+(rand()%52);
		while(*position1==0) position1=card+(rand()%52);
		position2=card+(rand()%52);
		while(*position2==0) position2=card+(rand()%52);
						
		//switch positions
		temp = *position1;
		*position1=*position2;
		*position2=temp;
		
		end=clock();
		position1=card;
		position2=card;
		timeout=(double)(end-start)/((double) CLOCKS_PER_SEC);
	}
}

void countdown(){
	printf("\nSERVER: Time out, clearing board\n");
	system("clear");
}



int main(){
	char cards[52]="aabbccddeeffgghhiijjkkllmmnnooppqqrrssttwwxxyyzz";
	int found[52];
	char mark_wrong[52];
	int player1_points,player2_points=0;
	int shmid;
	char *shm;
	key_t = 1234;
	int guess1,guess2,rb;
	int read[2];
	int turn;	//to know which client turn is	
	int i;	
	int fd_client1,fd_client2;
	signal(SIGALRM,countdown);	

	printf("\n*************************MEMORY GAME STARTED***************************\n");
	
	//create two pipes to read from clients
	mkfifo("Client1ToServer",0660);
	mkfifo("Client2ToServer",0660);
	
	fd_client1 = open("Client1ToServer",O_RDONLY);
	fd_client2 = open("Client2ToServer",O_RDONLY);
	
	
	//create a shared memory from where the values will be read
	if((shmid=shmget(key_t,SHMSIZ,IPC_CREAT|0666)) < 0){
		perror("Error in shmget");
		return 1;
	}
	
	//attach memory
	if((shm=shmat(shmid,NULL,0))== (char *) -1){
		perror("Error in shmat");
		return 1;
	}

	printf("\nSERVER:Waiting the two clients to be ready...\n");
	//wait until both clients are ready (clients will send "r" when ready
	while(*shm!="r" && (*shm+1)!="r"){
		sleep(1); //checks every seconds, otherwise there may be some read/write conflict
	}
	printf("\nSERVER:Clients ready, game can start!\n");

	printf("\nSERVER: Mixing cards...\n");
	mixcards(&cards[0]);
	

	//use the ready locations to now give turns to the clients t means it's that child turn, n means it's not its turn
	srand(getpid());		//change random seed
	turn=rand()%2+1;		//take either 1 or 2
	printf("\nSERVER: client %d will start first!\n");
	if(turn == 1){
		*shm="t";
		*shm+1="n";
	}
	else if(turn ==2){
		*shm="n";
		*shm+1="n";
	}
	
	while(player1_points <14 && player2_points <14){
		//It's client 1 turn
		if(turn == 1){
			printf("\nSERVER: Showing current situation:\nClient 1 Points: %d\t\t Client 2 Points: %d\n\n000000000011111111112222222223333333333444444444455\n0123456789012345678901234567890123456789012345678901\n",player1_points,player2_points);
			for(i=0;i<52;i++){
				if(found[i]!=0){
					printf("?");
				}
				else if(found[i]==1){
					printf("%c",cards[i]);
				}
			}
			printf("\n"); 	
			printf("\nSERVER: waiting for Client 1 response...\n");
			rb=read(fd_client1,&read[0],2*sizeof(int));
			if(rb<0) perror("\nPipe not reading correctly\n");
			guess1=read[0];
			guess2=read[1];
			
			//if client1 found a match
			if(cards[guess1]==cards[guess2]){
				printf("\nSERVER: WOW, Client 1 found a match!\n");
				found[guess1]=1;
				found[guess2]=1;
				player1_points++; 
			}

			//if client1 did not find a match
			else{
				alarm(10);
				
				//set string to highlight wrong positions
				for(i=0;i<51;i++){
					if(guess1 != i && guess2 != i){
						mark_wrong[i]=" ";
					}
					else{
						mark_wrong[i]="^";
					}
				}

				printf("\nSERVER: WHOPS, wrong match! Here's where:\n000000000011111111112222222223333333333444444444455\n0123456789012345678901234567890123456789012345678901\n");
				for(i=0;i<52;i++){
					if(found[i]!=0){
						printf("?");
					}
					else if(found[i]==1){
						printf("%c",cards[i]);
					}
				}
				printf("\n%s\n",mark_wrong);
			}

			//Update shared memory content so other client can proceed
			*shm="n";
			*shm+1="t";					
			turn=2;		//give turn to other client
		}
		//It's client 2 turn
		else{
		
			turn=1;		//give turn to other client
		}
		if(player1_points = 13 && player2_points ==13) break;		
	}


	//detach and delete shared memory
	shmdt(shm);
	shmctl(shmid,IPC_RMID,NULL);

	//close pipes
	close(fd_client1);
	close(fd_client2);

	//unlink pipes
	unlink("Client1ToServer");
	unlink("Client2ToServer");

	return 0;
}

