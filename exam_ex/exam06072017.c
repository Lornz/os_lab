#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals
#include <sys/msg.h>		//library used for msg queue commands
#include <time.h>		//library required to use function clock()
#include <pthread.h>		//library necessary to use threads
#include <semaphore.h>		//library necessary to use semaphores related functions
#include <math.h>		//library to allow usage of particular math functins eg pow(), to compile use gcc filename.c -lm -o filename.bin

#define READ_PORT 0
#define WRITE_PORT 1

float compute_avg(float in){
	return (in + (float) ((rand()%21)+10) )/2.0;
}

void killparent(){
	exit(0);
}

int main(){
	int p12[2],p23[2],p31[2];		//file descriptor for pipes, one where it reads, the other where it writes
	//int B;					//random generated number	
	int rb;					//value returned by read
	pid_t pid[2];
	
	//create the pipes
	pipe(p12);
	pipe(p23);
	pipe(p31);

	//create 2 children processes
	pid[0]=fork();
	pid[1]=fork();

	

	float average_w;			//value to write in pipe
	float A;				//value read from pipe all processes start with A=20(This will be average read)

	/*NOTE: The OS will close all the filedescriptors associated with the process that has died or exited so don't worry about
	the closing end of the pipe*/
	
	//1st child 23 connection
	if(pid[0] == 0){
		close(p23[READ_PORT]);
		close(p12[WRITE_PORT]);
		srand(getpid());
		while(1){
			rb=read(p12[READ_PORT],&A,sizeof(float));
			//if(rb<0) perror("read pipe error\n");
			average_w=compute_avg(A);
			printf("child 23: %f\n", average_w);
			if(average_w > 27) kill(getppid(),SIGUSR1);
			write(p23[WRITE_PORT],&average_w,sizeof(float));
		}
	}
	//2nd child 31 connection
	else if(pid[1] ==0){
		close(p31[READ_PORT]);
		close(p23[WRITE_PORT]);
		srand(getpid());
		while(1){
			rb=read(p23[READ_PORT],&A,sizeof(float));
			//if(rb<0) perror("read pipe error\n");
			average_w=compute_avg(A);
			printf("child 31: %f\n", average_w);
			if(average_w > 27) kill(getppid(),SIGUSR1);
			write(p31[WRITE_PORT],&average_w,sizeof(float));
		}				
	}
	//parent 12 connection
	else{
		close(p31[WRITE_PORT]);
		close(p12[READ_PORT]);
		signal(SIGUSR1,killparent);
		average_w=20;				//start writing 20 in the pipe
		write(p12[WRITE_PORT],&average_w,sizeof(float));		
		srand(getpid());

		while(1){
			rb=read(p31[READ_PORT],&A,sizeof(float));
			//if(rb<0) perror("read pipe error\n");
			sleep(1);
			printf("Parent A: %f",A);
			average_w=compute_avg(A);
			printf("parent 12: %f\n",average_w);
			if(average_w > 27) exit(0);			
			write(p12[WRITE_PORT],&average_w,sizeof(float));			
		}		
	}
	
}
