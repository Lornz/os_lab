#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>

//global synch variables
#define READ 0
#define WRITE 1

float rndavg(float input){
    return (input + (float) ((rand()%21)+15) )/2.0;

}

void sighandler(){
    exit(0);
}
int main(){
   pid_t child2,child3;

    int p12[2],p23[2],p31[2];
    //create 3 pipes
    pipe(p12);
    pipe(p23);
    pipe(p31);

    //fork
    child2=fork();
    child3=fork();
    float temp,newval;

    if(child2==0){ //child2
        close(p12[WRITE]);
        close(p23[READ]);
	srand(getpid());

        while(1){
            read(p12[READ],&temp,sizeof(float));
            newval=rndavg(temp);
            printf("Child2:\t%g\n",newval);
            if(newval>27)
                kill(getppid(),SIGUSR1);
            write(p23[WRITE],&newval,sizeof(float));
        }
    }else if(child3==0){ //child3
        close(p23[WRITE]);
        close(p31[READ]);
	srand(getpid());

        while(1){
            read(p23[READ],&temp,sizeof(float));
            newval=rndavg(temp);
            printf("Child3:\t%g\n",newval);
            if(newval>27)
                kill(getppid(),SIGUSR1);
            write(p31[WRITE],&newval,sizeof(float));
        }

    } else { //parent

        close(p31[WRITE]);
        close(p12[READ]);
        signal(SIGUSR1,sighandler);
        temp=15;
        write(p12[WRITE],&temp,sizeof(float));
	srand(getpid());

        while(1){
            int i=read(p31[READ],&temp,sizeof(float));
            sleep(1);
            newval=rndavg(temp);
            printf("Parent\t%g\n",newval);
            if(newval>27){
                exit(0);
            }
            write(p12[WRITE],&newval,sizeof(float));
        }
   

    }
}
