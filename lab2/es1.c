#include <stdio.h>
#include <sys/types.h>
int main(){
	int pid;
	for(int i=0;i<3;i++){ 
		pid=fork();
		if(pid==0){ //child
			printf("i'am a child. PID:%d\n",getpid());
			break; //exit from for loop
		}else{ //parent
			printf("i'am father. My PID:%d\tChild PID:%d\n",getpid(),pid);
		}
	}
}
