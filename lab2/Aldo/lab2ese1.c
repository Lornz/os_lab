#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
	pid_t child_pid;
	/*create a child process*/
	child_pid = fork();
	
	if(child_pid)
	{
		printf("Hi i'm the parent process with PID:%d and my child process is CH_PID:%dWho does create me?Nonno_Parent:%d",getpid(),child_pid,getppid());
	}else
	{
		printf("Hi i'm the child process CH_PID:%d \n",getpid());
	}

	printf("it's correct\n");
	while(1);

}

