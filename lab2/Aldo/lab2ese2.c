#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main()
{
	/* code */
	pid_t pid;
	for (int i = 0; i < 3; ++i)
	{
		if ((pid=fork()) == 0)
		{	
			printf("I'm a child with pid=%d and my parent is=%d\n",getpid(),getppid());
			break;	/* code */
		}
	}
	
	return 0;
}
