#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main()
{
	/* code */
	pid_t pid;
	printf("Hi i'm the parent:%d\n",getpid() );
	if ((pid=fork()) == 0)
	{
		printf("Hi i'm the child:%d and my parent is:%d\n",getpid(),getppid());
		if ((pid=fork()) == 0)
		{
			printf("Hi i'm the child:%d and my parent is:%d\n",getpid(),getppid());
			if ((pid=fork()) == 0)
			{
				printf("Hi i'm the child:%d and my parent is:%d\n",getpid(),getppid());
			}
		}

	}
	return 0;
}
