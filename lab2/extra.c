#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

int main(){
	int value;
	int pid;
	int status;
	int retval;
	pid= fork();
	if(pid==0){
		printf("child\n");
		value+=15;
		exit(0);
	}else{
		printf("parent\n");
		sleep(5);
		retval=wait(&status);
		printf("ret value:%x\tstatus:%x",retval, status);
	}
}
