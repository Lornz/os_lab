#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <math.h> //had to include this library to compute the power
//to compile in UNIIX: gcc ex3.c -lm -o ex3 is needed



int main()
{
	int status,temp,i=0;
	int n = 5;
	int pid;
	int output=0;
	for (i=0;i<n+1;i++){
		pid=fork(); //allows child process creation
		if(pid==0){/*Only if child then trapped by while */
			printf("This is child number: %d \n", i+1);			
			status= (int) pow(2, i);		
			printf("Child status value: %d \n", status);
			sleep(2);
			exit(status); //child terminates passing the status to the parent process	
		}
		wait(&status); //parent realizes the child has terminated
		if( WIFEXITED(status)){ //wifexited returns a boolean
			printf("Parent returned status value: %d \n", WEXITSTATUS(status));
			output +=WEXITSTATUS(status); //WEXITSTATUS selects the 8 LSB from exit!
			printf("The current output value is: %d	\n", output);
		}
	}
return 0;
}
	
