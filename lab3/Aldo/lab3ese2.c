#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

int main()
{
	int status,i,cnt=0,k=0,rad_val;
	int pid[3];
	int pid_sec;
	for (i=0;i<3;i++){
		pid[i]=fork();
		if(pid[i]==0){
			while(1) {   /*Only if child then trapped by while */
				rad_val = rand();
				sleep(10); //not asked but placed to avoid too many prints on the screen
			}
		}
	}
	while(1){
		while(cnt<3){ //loops until cnt less than 3
			pid_sec=fork();
			if(pid_sec==0){
				while(k<50){
					rad_val=rand();
					printf("Child  randomly generated %d-th number: %d \n", k,rad_val);
					printf("Child  randomly generated squared %d-th number: %d \n", k,rad_val^2);
					k++;
				}
				/*useless to put k=0 because that refers just to the child */
				status=666;
				sleep(2);
				exit(status); //Child terminates and the process is deleted
			}
			cnt++;
		}
		wait(&status); //To greve the child's death		
		if( WIFEXITED(status)){ //if the child correctly terminated (returning a boolean)
			cnt--; //this allows to regenerate the for loop and create new children
		}
	}
return 0;
}
	
