#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
int main()
{
	/* code */
	pid_t pid[2];
	int status;
	printf("i'm the parent process with PID=%d \n",getpid() );
	for (int i = 0; i < 2; ++i)
	{
		if((pid[i] = fork()) == 0)
		{
			printf("child_porcess %d sleeps for 5 seconds\n",getpid() );
			sleep(5);
			printf("child_porcess %d exits with success\n",getpid() );
			exit(0);
		}
	}
	waitpid(pid[1],&status,0);
	printf("Parent_Process is termianted\n");
	exit(0);
	return 0;
}