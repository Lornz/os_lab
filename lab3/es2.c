#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
int main(){
	pid_t pid1[3],pid2[3];
	int status;
	int type=0;
	for(int i=0;i<5;i++){
		if(i<3){
			pid1[i]=fork();
			if(pid1[i]==0){
				type=1;
				break;
			}
		}
		else{
			pid2[i-3]=fork();
			if(pid2[i-3]==0){
				printf("pid:%d\n",getpid());
				type=2;
				break;
			}
		}
	}
	if(type==1){
//		while(1){
			sleep(1);
			int val=rand();
			printf("%d\n",val);
//		}
	}
	if(type==2){
		sleep(2);
		printf("second\t%d\n",getpid());
		for(int i =0;i<49;i++){
			int val=rand();
			printf("%d\t%d\n",val, val*val);
		}
		exit(0);
	}		
	
}
