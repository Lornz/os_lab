#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
int main(){
	pid_t first,second;
	first=fork();
	int status;
	if(first==0){
		sleep(5);
		exit(20);
	}
	second=fork();
	if(second==0){
		sleep(5);
		exit(20);
	}
	waitpid(second, &status, 0);
	printf("Parent finished. Exit status:%u",WIFEXITED(status));
}
