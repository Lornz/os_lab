#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/signal.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHMSZ 10*sizeof(char)
#define READ 0
#define WRITE 1

char sigusr_flag;

void alarm_hnd(int signo){
	//send SIGUSR1 to father
	kill( getppid(), SIGUSR1);
	//reset the alarm
	alarm(10);
	return;
}

void sigusr_hnd(int signo){
	printf("Father received SIGUSR1\n");
	sigusr_flag=1;
	return;
}


int main(int argc, char **argv){
	//pipe
	int fd[2];
	//pid child
	pid_t pid;
	//creiamo la pipe
	pipe(fd);
	//fork
	pid=fork();
	sigusr_flag=0;
	
	//child
	if(pid==0){
		close(fd[READ]);
		signal(SIGALRM, alarm_hnd);
		char outn;
		//set the first alarm
		alarm(10);	
		while(1){
			outn = (char) (rand()%31);
			write(fd[WRITE], &outn, 1);
			sleep(1);
		}
		close(fd[WRITE]);
		exit(0);
	



	//father	
	} else {
		signal(SIGUSR1, sigusr_hnd);
		char exit_key=0;
		char input_value;
		close(fd[WRITE]);
		char running=1;
		while(running){
			if(sigusr_flag!=0){
				sigusr_flag=0;
				exit_key=rand()%31;
			}
			read(fd[READ], &input_value, 1);
			printf("%u\texitkey:%d\n", input_value, exit_key);
			if(input_value==exit_key)
				running=0;
		}
		close(fd[READ]);
		return 0;
	}

	return 0;
}	
