#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/signal.h>
#include <sys/shm.h>
#include <unistd.h>
#include <string.h>

#define SHMSZ 10*sizeof(char)
#define READ 0
#define WRITE 1

char sigusr_flag;

void alarm_hnd(int signo){
	//send SIGUSR1 to father
	kill( getppid(), SIGUSR1);
	//reset the alarm
	alarm(10);
	return;
}

void sigusr_hnd(int signo){
	sigusr_flag=1;
	return;
}


int main(int argc, char **argv){
	//pipe
	int fd[2];
	//pid child
	pid_t pid;
	//creiamo la pipe
	pipe(fd);
	//fork
	pid=fork();
	sigusr_flag=0;
	
	//child
	if(pid==0){
		close(fd[READ]);
		signal(SIGALRM, alarm_hnd);
		char outn[3];
		//set the first alarm
		alarm(10);	
		while(1){
			sprintf(outn,"%d",rand()%3);
			write(fd[WRITE], outn, sizeof(outn));
			sleep(1);
		}
		close(fd[WRITE]);
		exit(0);
	



	//father	
	} else {
		signal(SIGUSR1, sigusr_hnd);
		char exit_key[3];
		char input_value[3];
		close(fd[WRITE]);
		char running=1;
		while(running){
			if(sigusr_flag!=0){
				sigusr_flag=0;
				sprintf(exit_key,"%d",rand()%3);
			}
			read(fd[READ], input_value, sizeof(input_value));
			printf("> %s\n", input_value);
			if(strcmp(input_value,exit_key)==0)
				running=0;
		}
		close(fd[READ]);
		return 0;
	}

	return 0;
}	
