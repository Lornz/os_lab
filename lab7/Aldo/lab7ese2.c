#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/signal.h>
#include <sys/shm.h>
#include <unistd.h>
#include <wait.h>



void sig_quit_hand(int signo){
	printf("DOH! I'm Child %d and i was killed\n",getpid() );
	
	kill(getpid(),SIGINT);
	return;
}





int main(int argc, char **argv){
	pid_t pid[10];
	int child_to_kill;
	int N;
	int command=-1;
	int cnt=0;
	

	for (int i = 0; i < 3; i++)
	{
		pid[i]=fork();
		if(pid[i] == 0)
		{
			int rpt=rand()%11;
			signal(SIGQUIT,sig_quit_hand);
			while(1){
				sleep(rpt);
				//printf("Child_porcess %d my parent is: %d \n>\t",getpid(), getppid());
			}
			
			//exit(0);
			

		}
		//printf("%d\n", pid[i]);
		cnt++;
	}
 	printf("Cnt Value :%d\n",cnt );
	while(cnt>0)
	{
		
		printf("\nMenu:\n");
		printf("1.Visualize Children Process\n");
		printf("2.Kill Child process\n");
		printf(">>\t");
		scanf("%d",&command);

		switch(command)
		{
			case 1:
			printf("Children:\n");
				for (int i = 0; i < 3; ++i)
				{
					if(pid[i] != 0)
					{
						printf("%d \t",pid[i] );
					}
				}
			break;

			case 2:
			printf("Child PID to kill>\t");
			scanf("%d",&child_to_kill);

			for (int i = 0; i < 3; ++i)
				{
					if(child_to_kill == pid[i])
					{
						kill((pid_t) child_to_kill,SIGQUIT);
						pid[i]=0;
						cnt = cnt -1;
						break;
					}
					if(i == 3)
					{
						printf("No such child found\n");
						break;
					}
				}
			break;

		}

	}

	return 0;
}	
