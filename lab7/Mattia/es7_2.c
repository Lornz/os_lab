#include <fcntl.h>		//library to use some flags such as P_RDONLY etc
#include <sys/stat.h>		//this library allows to create a named pipe, aka fifo, through mkfifo
#include <sys/types.h>		//library used for threads, keys (key_t), process ids (pid_t) etc.
#include <unistd.h>		//library that allows access to POSIX apis
#include <stdio.h>		//library for standard input output functons
#include <string.h>		//library that contains some useful functions to handle strings such as strcat, strcopy etc.
#include <stdlib.h>		//library to declare functions and constats of global utility: it's a C standard library
#include <sys/wait.h>		//this is to handle wait system call
#include <errno.h>		//this is to handle error messages
#include <sys/ipc.h>		//library allowing inter process communication 
#include <sys/shm.h>		//this is to handle shared memories
#include <signal.h>		//library used to handle signals


pid_t pid_to_kill;	//This will be the user defined pid to kill

void quit_sig(int signo){
	if(signo == SIGQUIT){
		signal(SIGQUIT, SIG_DFL);
		kill(pid_to_kill,SIGQUIT);
	}
	//signal(SIGQUIT, SIG_DFL); //This will reinstall the default signal handler for SIGQUIT
	//kill(pid_to_kill,SIGQUIT);
}



int main(){

	pid_t pid[10]; 		//This will store the process id
	int running=1;
	int i=0;
	int sleep_time;		//This will be the random amount of time the children will sleep
	int counter=0;

	//generate the 10 children processes
	for(i=0; i<10; i++){
		printf("Generating child %d\n",i+1);
		pid[i]=fork();
		if(pid[i]==0) break;
	}

	//if it's the child
	if(pid[i]==0){
		sleep_time = rand()%10 +1;
		while(1){
			sleep(sleep_time);
			printf("Hi, I'm child %d and I am depressed, please kill me! Existance is pain!\n", getpid());
			sleep_time= rand()%10+1;
		}	
		//pause(); 	//suspends execution until a signal is delivered
		exit(0);
	}
	//if it's the parent
	else{
		while(running){
			signal(SIGQUIT, quit_sig);			//this will change the deafault signal handler for SIGQUIT
			sleep(1);
			printf("My Children pid are the following: \n");
			for(i=0; i<10; i++){
				if(pid[i]!=0){
					printf("Child %d with pid: %d\n",i+1, (int)pid[i]);	//This is to display all the children pids
				}
			}
			printf("Insert the child pid you want to kill\n");
			scanf("%d",&pid_to_kill);				//ask the user for the pid to kill as long as pid<=0 	
			printf("Called the SIGQUIT signal for process %d\n", pid_to_kill);
			printf("Killing the process %d \n", pid_to_kill);
			kill(pid_to_kill,SIGQUIT);
			for(i=0; i<10; i++){
				if(pid[i]==pid_to_kill){ 
					pid[i]=0;
					counter +=1;
				}
			}
			if(counter == 10) running=0; //This is if all children processes have been killed					  
		}
	}
		
	wait(NULL); //child dead and can be grieved
	printf("Closing the program\n");
		
	return 0;	
}


