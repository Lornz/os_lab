#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <semaphore.h>
#include <time.h>

//global synch variables
#define READ 0
#define WRITE 1

#define SENSOR_FILE "sensor.txt"

//global variables
sem_t *written,*reads;

//this function just create a random number
float sensor(){
    return (float) (rand()%100);
}

//check the 3 values, get directly the buffer
char check(float *array){
    float *farr = array;
    float avg;
	//mantains the value from one run to the other
    float static past_avg=0;
    char retval;

	//perform average
    avg=(farr[0]+farr[1]+farr[2])/3.0;
	//print some values
	printf("read\t%g\t%g\t%g\navg:%g\tpast_avg:%g\n",farr[0],farr[1],farr[2],avg,past_avg);

	//not the first run
    if(past_avg){
		//return the check result (boolean)
        retval=( avg > 2.0*past_avg );
		//update the value
        past_avg=avg;
        return retval;
    }
	//first run
	//do not compare, just update
	past_avg=avg;
    return 0;

}
int main(){
    int shmid;  //shared memory id
    int fd;     //file descriptor

    //create shared memory
    shmid=shmget(112983,2*sizeof(sem_t),IPC_CREAT|0644);
	//attach to shared 
	void *shmpt=shmat(shmid,NULL,0);
	//initialize semaphores
	written=shmpt;
	reads=shmpt+sizeof(sem_t);
	
	if(sem_init(written,1,0))
		perror("Sem written");
	if(sem_init(reads,1,1))
		perror("Sem read");


    //fork
    pid_t reader=fork();


    if(reader==0){ //reader
		
		//open the sensor file
        fd=open(SENSOR_FILE, O_CREAT|O_RDONLY);
        float buff[3];

        
        while(1){
			//wait for the writer
            sem_wait(written);
            sem_wait(written);
            sem_wait(written);

            read(fd,buff,3*sizeof(float));

            //check
            if(check(buff)){
				
                printf("exiting and killing\n");
				//patricide
                kill(getppid(),SIGINT);
                sem_destroy(written);
                sem_destroy(reads);
				//suicide
                exit(0);
            } 
			//signal the writer
            sem_post(reads);
        }
    
    } else { //writer

		//seed the random number generator. JUST once
		srand(time(NULL));

        fd=open(SENSOR_FILE, O_CREAT|O_WRONLY);
        if(fd<0)
            perror("cannot open sensor.txt");
        while(1){

            sem_wait(reads);

            //generate rand and write
            float buff[3];
            buff[0]=sensor();
            buff[1]=sensor();
            buff[2]=sensor();
            write(fd,buff,3*sizeof(float));
            printf("written\t%g\t%g\t%g\n",buff[0],buff[1],buff[2]);

            sem_post(written);
            sem_post(written);
            sem_post(written);
        }
        
    }
}
