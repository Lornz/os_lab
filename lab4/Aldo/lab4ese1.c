#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/wait.h>


int main()
{
	int pid;
	int status;
	char  pidconvertito[10];
	char * command;
	command =(char *)malloc(50*sizeof(char));
	while(1)
	{
		printf("\nMy-Shell>\t");
		scanf("%s", command);
		//printf("%s\n",command );

		if(strcmp(command, "exit") == 0){
			printf("********   My-Shell Closed   ********\n");
			exit(0);
		}else
			{
				pid = fork();
				if(pid < 0){
					printf("Error in Child Generation:%s\n", strerror(errno) );
					exit(1);
							}

				if(pid == 0){
						/*child process*/

					
					strcat(command, " > ");
					snprintf(pidconvertito,10,"%d.log",(int)getpid());
					strcat(command,pidconvertito);
					strcat(command, " &");
					//printf("%s",command);
					execl("/bin/bash","bash","-c",command,(char *) 0);
					printf("Failed Exec \n");
					exit(-1);
					}else{


						/*parent process*/

						waitpid(pid,&status,0);
						if (WIFEXITED(status))
						{
							/* child process terminated qith success */
							printf("Child %d terminated. Status:%d\n",pid,WEXITSTATUS (status));
						}else{
							/*child process exit with -1 so abnormal error-->child han not been executed*/
							printf("Abnormal termination:%s\n",strerror(errno) );
						}

					}

			}

	}
}