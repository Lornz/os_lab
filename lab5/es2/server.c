#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define MSGSIZE 511

struct msg_t {
	long int type;
	char payload[MSGSIZE+1];
};

int main(){

	printf("SERVER :)\n");

	
	key_t key=1234;
	struct msg_t msg;
	long int type=1;
	long int msgid;
	msgid= msgget( key, 0666 | IPC_CREAT);
	
	if(msgid==-1){
		perror("message que:");
		return -1;
	}
	
	while(1){
		if(msgrcv(msgid, &msg, MSGSIZE, type, 0) == -1 ){
			perror("msgrcv");
			exit(EXIT_FAILURE);
		}
		printf("\tServer received: %s\n", msg.payload);
	}
}

