#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define MSGSIZE 511
#define BUFFSIZE 255

struct msg_t {
	long int type;
	char payload[MSGSIZE+1];
};

int main(){
	struct msg_t msg;
	int msgid;
	key_t key=1234;
	char header[]="\n$ ";
	char buff[BUFFSIZE];

	//create queue	
	msgid= msgget( key, 0666 | IPC_CREAT);
	
	if(msgid==-1){
		perror("message que:");
		return -1;
	}

	if( fork()==0){
		execl("/home/lorenzo/OS_lab/lab5/es2/server.bin", "server.bin", NULL);
		printf("Server failed to start\n");
	}
	while(1){

		printf("%s", header);
		scanf("%s",buff);
		
		if( buff=="exit")
			return 0;

		msg.type=1;	
		strcpy(msg.payload, buff);
		int len=strlen(msg.payload) +1;
		
		if( msgsnd( msgid, &msg, len, 0) == -1){
			perror("msgsnd:");
			exit(-1);
		}
	}
		
}
