#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/msg.h> //it is added to use msg queue commands
#include <errno.h>
#include <sys/wait.h>
#include <string.h>

#define MAX_TEXT 512

struct my_msg_snt //DATA STRUCTURE CREATION
{
	long int type;
	char message[MAX_TEXT];
};


int main(){

	int running = 1; //Variable equal to 1 as long as server listens
	int msgid; //value returned by msgget();
	struct my_msg_snt some_data;
	int len;//length of the final message
	char buffer[BUFSIZ];//static allocation of char buffer 
	int mykey =getuid(); //getuid returns the real user id of the calling process

	msgid = msgget((key_t)mykey, 0666 | IPC_CREAT);//the message queue has been created
	if(msgid == -1)
	{
		fprintf(stderr, "msgget failed with error %s\n",strerror(errno) );
		exit(EXIT_FAILURE);
	}

	some_data.type = 1;//define the type of message



	while(running)
	{
		printf("Enter Command > \t");
		fgets(buffer,BUFSIZ,stdin);
		strcpy(some_data.message,buffer);

		len = strlen(some_data.message) + 1;//+1 is for string terminator

		if (msgsnd(msgid,&some_data, len,0) == -1)
		{
			fprintf(stderr, "msgsnd failed with error %s\n",strerror(errno) );
			exit(EXIT_FAILURE);
		}

		if (strncmp(some_data.message,"exit",4) == 0)
		{
			running = 0;//exit from while
		}

	}

	exit(EXIT_SUCCESS);

}

