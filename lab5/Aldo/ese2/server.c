#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
telegr#include <errno.h>
#include <sys/wait.h>
#include <string.h>

struct my_msg_rcv //DATA STRUCTURE CREATION
{
	long int type;
	char message[BUFSIZ];
};


int main(){

	int running = 1; //Variable equal to 1 as long as server listens
	int msgid; //value returned by msgget();
	struct my_msg_rcv some_data;
	long int type_to_take = 0; //is the long msgtype of the msgrcv syscall; =0 means that the message is the first available in the FIFO
	int mykey =getuid(); //getuid returns the real user id of the calling process

	int return_sys_value;//return value of system();

	msgid = msgget((key_t)mykey,0666 | IPC_CREAT);//the message queue has been created

	if(msgid == -1)
	{
		fprintf(stderr, "msgget failed with error %s\n",strerror(errno) );
		exit(EXIT_FAILURE);
	}


	while(running)
	{
		if (msgrcv(msgid,&some_data, BUFSIZ, type_to_take,0) == -1)
		{
			fprintf(stderr, "msgrcv failed with error %s\n",strerror(errno) );
			exit(EXIT_FAILURE);
		}


		printf("Message received: %s \n", some_data.message);
		strcat(some_data.message, " &");

		return_sys_value = system(some_data.message);
		//usare la exec normale se system d non riesce ad eseguire in background un processo.
		
		if (strncmp(some_data.message,"exit",4) == 0)
		{
			running = 0;//exit from while
		}

	}



	if(msgctl(msgid,IPC_RMID,0) == -1)
	{
		fprintf(stderr, "msgctl(IPC_RMID) failed with error %s\n",strerror(errno) );
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);

}

