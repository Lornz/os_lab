#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHMSZ 6*sizeof(float) 

//function to compute the power
float intpow(float x, int e){
	float pow=1;
	printf("res=%g\n",pow);
	for(int j=0;j<e;j++){
		pow*=x;
	}
	return pow;
}

int main(){
	float x;   //value to be asked to the user
	int N;     //value to be asked to the user (must be lower than 5)
	int shmid; //value returned by shmget, that will be our shared memory id number
	//key_t key; //value corresponding to the key to access the shared memory (not needed for parent-child relat.)
	//we can use as key IPC_PRIVATE: with it the shared mem can be accessed only by its creating process or
	//a child of that process
	char *shm; //this will be the pointer to the shared memory
	float *s;  //pointer to shared memory will be casted into float pointer	
	pid_t pid; //this will store the pid of parent/child
	int i;	   //index for the child creation
	float temp; //variable storing temporary result of the operation;	
	float res=0; //variable storing the final result of the operation

	printf("Write the x value \n");
	scanf("%f", &x); //assign inserted value to x
	printf("Write the N<=5 value \n");
	scanf("%d", &N);
	while(N>5){	
	printf("Write the N<=5 value \n");
	scanf("%d", &N);
	} 

	//Shared memory creation

	if( (shmid = shmget(IPC_PRIVATE, SHMSZ, IPC_CREAT | 0666)) <0 ){
		perror("Error in shmget");
		exit(1);
	}
	
	//Memory attachment

	if( (shm = shmat(shmid, NULL, 0)) == (char *) -1 ){
		perror("Error in shmat");
		exit(1);
	}

	//Child process generation

	printf("Generationg child processes...\n");
	for(i=0;i<=N;i++){
		if( (pid=fork()) ==0){
			printf("done\n");	
			break;
		}
	}
	
	
	//This is where things get nasty...

	if(pid==0) { 
	//child process
		s = (float *) shm; //casting pointer to float
		s += i*sizeof(float); //adds to the previous casted address (same for all childern) the corresponding size of float*i
		temp=intpow(x,i); //compute the operation
		*s = temp; //store partial result in address pointed by s (dereferencing)
		printf("child %d ended \n",i);
		exit(0);
	}
	else {
	//parent process
		//La sleep non è la miglior soluzione che si possa avere sarebbe meglio implementare un sistema con flag.
		
		sleep(5);   //sleeps should provide enough time for the children to complete their job
		wait(NULL); //To greve a single child death
		for(i=0;i<=N;i++){
			s = (float *) shm;
			res += * (s + i*sizeof(float)); //sum the content pointed by right argument
		}
		printf("The final result is: %g\n", res);
		exit(0);
	}	
}

