#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHMSZ 6*sizeof(float)+6
//0 to 5 + 6 ending flags
float intpow(float x, int e){
	float res=1;
	for(int i=0;i<e;i++){
		res*=x;
	}
	return res;
}

int main(int argc, char **argv){
/*
server: call shmget that creates the the shared memory, return shmid;
you pass the key used to access to the shared memory 
- IPC_CREATE flag for creating the segment
- set permission eg 0666

use shmid to pass it to shmat
shmat(smget(...))
	- shmid
	- NULL: no preference for starting point of shared memory
		the OS decides it
	- 0 : non so
shmat return a pointer to the shared memory

Shared memory can result in  a race condition
Here the synch is done with server waiting for the first character to become '*'
*/

/*
CLIENT
use the same key to shmget (same permission)
use shmat that return the pointer
the client read from shared memory and write to stdout
*/
	float x;
	int N;

	int shmid;
	key_t key;
	char *shm;
	key=666;
	pid_t pid;
	int i;
	
	printf("scrivere x e poi N<=5\n");
	scanf("%f",&x);
	scanf("%d", &N);
	while (N>5){
		printf("scrivere N<=5");
		scanf("%d", &N);
	}
	//create shared memory
	if( (shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) <0){
		perror("shmget");
		return 1;
	}
	//attach 
	if( (shm = shmat(shmid, NULL, 0)) == (char *) -1){
		perror("shmat");
		return 1;
	}
	//generate child that inherit x,i,shm
	for(i=0;i<=N;i++){
		if( (pid=fork()) == 0 ){
			break;
		}
	}
	if(pid==0){
	//child
		float temp;
		temp= intpow(x,i);
		float *res;
		//res= (float *) (shm + i*sizeof(float));	
		res= (float *) (shm+i*sizeof(float));	
		*res=temp;
		//write the DONE flag
		shm[6*sizeof(float)+1+i]=0xff;	
		exit(0);	
	} else{
	//father
		//starting value
		wait(NULL);
		char flag=0;
		while(flag==0){
			//bitwise AND of all flags;
			flag=0xff;
			for(i=0;i<=N;i++)
			{
				flag&= shm[6*sizeof(float)+1+i];
				//printf("%d",shm[5*sizeof(float)+1+i]);
			}
			//putchar('\n');
		} //exit when all child ended
		
		//perfo	exit(0);rm the sum
		float sum=0;
		for(i=0;i<=N;i++){
			//cast to pointer of float, than read that location
			sum+=  ((float*) (shm))[i];
		}
		printf("\nSum: %g\n", sum);	
		return 0;
	}
	printf("Father: I'm lost\n%d\t%d", getpid(), pid);

}	
