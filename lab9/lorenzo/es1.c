#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>
pthread_mutex_t sum_mutex;
float sum;



void *sumfunction(void* idptr){
	unsigned int id= (unsigned int) idptr;
	float temp= 1.0 / ( (float) (1<<id));
	
	pthread_mutex_lock(&sum_mutex);
	//critical section
	sum+=temp;
	pthread_mutex_unlock(&sum_mutex);
	
	pthread_exit(NULL);
}
int main(int argc, char *argv[]){

	int thread_number=atoi(argv[1]);
	printf("%d %d\n",argc,thread_number);
	
	//allocate array of id
	pthread_t *thr_id=malloc(sizeof(pthread_t)*(thread_number+1));
	//create mutex
	pthread_mutex_init(&sum_mutex, NULL);

	//create threads
	for(int i=0;i<=thread_number;i++){
		int retval=pthread_create(&thr_id[i],NULL, sumfunction, (void *) i);
		if(retval)
			printf("Error creating threads\n");
	}
	//join threads
	for(int i=0;i<=thread_number;i++){
		pthread_join(thr_id[i],NULL);
	}
	
	printf("sum: %20f", sum);
	printf("Father exited\n");
	
	pthread_mutex_destroy(&sum_mutex);
	free(thr_id);
	return 0;
}
