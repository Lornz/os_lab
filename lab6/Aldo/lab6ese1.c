#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>


#define READ_PORT 0
#define WRITE_PORT 1
#define e 2.71828183

float intpow(float x, int k){
	float pow=1;
	for(int j=0;j<k;j++){
		pow*=x;
	}
	return pow;
}



int main(){
	int N;
	int fd[2];//file descriptor for file creation
	pid_t pid;
	float tmp;
	float tmp1;
	float res=0;//final result initialized to 0


	printf("Write the N<=10 value \n");
	scanf("%d", &N);
	while(N>10){	
	printf("Write the N<=10 value \n");
	scanf("%d", &N);
	} 

	if(pipe(fd) == -1)
	{
		fprintf(stderr, "Pipe Failed\n");
		return 1;
	}//create unamed pipe
	
	/*fork for N children processes*/
	for (int i = 1; i < N; i++)
	{
		//printf("Ciclo ***************%d****************\n",i);
		pid = fork();
		if (pid == 0) //child writer
		{
			close(fd[READ_PORT]);//close read port beacuse child has only to write
			tmp = intpow(e,i);
			tmp = tmp / i ;
			
			write(fd[WRITE_PORT],&tmp,sizeof(tmp));
			close(fd[WRITE_PORT]);
			exit(0);
		}
		
	}

	/*parent process*/
			close(fd[WRITE_PORT]);
			sleep(1);
			while(read(fd[READ_PORT],&tmp1,sizeof(float)) != 0 )
			{
				printf("%p\t",lseek(fd[READ_PORT],0,SEEK_CUR) );
				printf("Res_partial:%f \n",tmp1 );
				printf("%p\n",lseek(fd[READ_PORT],0,SEEK_CUR) );
				res=res+tmp1;
			}
			printf("Res_total:%f \n",res );

	wait(NULL);
	close(fd[READ_PORT]);
	//printf("\nResult: %f\n",res);
	return 0;
	
}

