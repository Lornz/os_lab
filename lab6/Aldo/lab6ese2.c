#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>


#define READ_PORT 0
#define WRITE_PORT 1
#define e 2.71828183

float intpow(float x, int k){
	float pow=1;
	for(int j=0;j<k;j++){
		pow*=x;
	}
	return pow;
}

unsigned long factorial(int denom)
{
	if(denom == 0)
	{
		return 1;
	}else{
		return denom*factorial(denom -1);
	}
}


int main(){
	int N;
	float x;
	int fd[2];//file descriptor for file creation
	pid_t pid;
	float tmp;
	float res=1;//final result initialized to 0
	char tmpstring[10];
	char tmpstring1[10];

	
	
    printf("Write the x value \n");
	scanf("%f", &x);



	printf("Write the N>=0 value \n");
	scanf("%d", &N);
	while(N<0){	
	printf("Write the N>=0 value \n");
	scanf("%d", &N);
	} 


	


	char * c=(char *)malloc(N*10);//dynamic allocationin heap memory
	//char message[100];
	char* p=c;

	if(pipe(fd) == -1)
	{
		fprintf(stderr, "Pipe Failed\n");
		return 1;
	}//create unamed pipe
	
	/*fork for N children processes*/
	for (int i = 1; i <= N; i++)
	{
		//printf("Ciclo ***************%d****************\n",i);
		pid = fork();
		if (pid == 0) //child writer
		{
			close(fd[READ_PORT]);//close read port beacuse child has only to write
			tmp = intpow(x,i);
			tmp = tmp / factorial(i) ;
			printf("tmp di child %d: %f \n",(int)getpid(),tmp);
			strcpy(tmpstring1, " ");
			snprintf(tmpstring,10,"%f",tmp);//converts float to tring adding the termination term '\0'
			strcat(tmpstring1,tmpstring);
			write(fd[WRITE_PORT],tmpstring1,strlen(tmpstring1));
			close(fd[WRITE_PORT]);
			exit(0);
		}
		
	}

	/*parent process*/
			close(fd[WRITE_PORT]);
			sleep(1);
			read(fd[READ_PORT],c,N*10);
			//printf("seek_position: %d\n",(int) lseek(fd[READ_PORT],0,SEEK_END));
			//printf("parent: message--> %s\n",message);

			while (*p)
			{
			    if (*p++ == ' ')
			    	//printf("\n%f",atof(p));
			    	res=res+atof(p);
			        
			}
			
			


	wait(NULL);
	close(fd[READ_PORT]);
	printf("\nResult: %f\n",res);
	free(c);//deallocate heap memory
	return 0;
	
}

