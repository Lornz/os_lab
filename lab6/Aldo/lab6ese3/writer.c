#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_BUF 256

int main()
{
    int fd;
    char * myfifo = "/tmp/myfifo";
    char cmd[MAX_BUF];

    /* create the FIFO (named pipe) */
    mkfifo(myfifo, 0666);



    /* write "Hi" to the FIFO */
    fd = open(myfifo, O_WRONLY);
    while(1)
    	{
    		printf("Insert_command-->\t");
    		fgets(cmd,MAX_BUF,stdin);//the trick isto use fgets to 
            //capture command in input. Indeed fgets catch stdin till 
            //encounter \n. In this way cmd is ever the what we have write by command line
    		write(fd, cmd, sizeof(cmd));
    		
    		if(strcmp(cmd,"exit\n") == 0)
	    		{
	    			sleep(1);
	    			printf("Exit\n");
	    			close(fd);
					unlink(myfifo);
	    			exit(0);
	    		}

    	}

}