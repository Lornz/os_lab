#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_BUF 256

int main()
{
    int fd;
    char * myfifo = "/tmp/myfifo";
    char buf[MAX_BUF];

    /* open, read, and display the message from the FIFO */
    fd = open(myfifo, O_RDONLY);

    while(1)
    {
    	read(fd, buf, MAX_BUF);
    	/*After read has been read from pipe a message of MAX_BUF length delete all in the pipe*/
    	if (strcmp(buf,"home\n") == 0)
    	{
    		printf("Home Detected\n");
    	}else if (strcmp(buf,"exit\n") == 0)
    	{
    		close(fd);
    		printf("Exit\n");
    		exit(0);
    	}
    	printf(">%s\n",buf );
    	//memset(buf, 0, sizeof buf);
    }

}