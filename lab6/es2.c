#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHMSZ 6*sizeof(float)+6
#define EUL 2.718281
#define READ 0
#define WRITE 1
float intpow(float x, int e){
	float res=1;
	for(int i=0;i<e;i++){
		res*=x;
	}
	return res;
}

int main(int argc, char **argv){

	float x;
	int N;
	if (argc>=3){
		x = atof(argv[1]);
		N=atoi(argv[2]);
	} else {
		printf("pass N as argument\n");
		return -1;
	}
	
	//2: 		read write
	//N: 		N processes have N pipes
	//sizeof(int):	size of the fd
	int* fd=malloc( 2 * N * sizeof(int));
	int i=0;
	pid_t pid;
	
	//generate N child and pipes
	for (i; i<N;i++){
		pipe(fd+2*i);
		//child
		if((pid=fork())==0){
			break;
		}
	}
	
	//child
	if(pid==0){
		float temp;
		if(i!=0){
			long int factorial=1;
			for(int j=1; j<=i; ++j)
			{
			    factorial *= j;              // factorial = factorial*i;
			}
			temp=intpow(x,i)/((float)(factorial));
		} else {
			temp=1;
		}
		write(fd[2*i+ WRITE], &temp, sizeof(float));
		close(fd[2*i+WRITE]);
		close(fd[2*i+READ]);	
	//father	
	} else {
		float sum=0,temp=0;
		for(int i = 0; i<N; i++){
			read(fd[2*i+READ], &temp, sizeof(float));
			sum+=temp;	
			close(fd[2*i+WRITE]);
			close(fd[2*i+READ]);	
		}
		printf("Sum: %f\n", sum);
	}

	free(fd);
	return 0;
}	
