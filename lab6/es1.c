#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHMSZ 6*sizeof(float)+6
#define EUL 2.718281
#define READ 0
#define WRITE 1
float intpow(float x, int e){
	float res=1;
	for(int i=0;i<e;i++){
		res*=x;
	}
	return res;
}

int main(int argc, char **argv){
/*
server: call shmget that creates the the shared memory, return shmid;
you pass the key used to access to the shared memory 
- IPC_CREATE flag for creating the segment
- set permission eg 0666

use shmid to pass it to shmat
shmat(smget(...))
	- shmid
	- NULL: no preference for starting point of shared memory
		the OS decides it
	- 0 : non so
shmat return a pointer to the shared memory

Shared memory can result in  a race condition
Here the synch is done with server waiting for the first character to become '*'
*/

/*
CLIENT
use the same key to shmget (same permission)
use shmat that return the pointer
the client read from shared memory and write to stdout
*/
	int N;
	if (argc>=2){
		N=atoi(argv[1]);
		if(N>10){
			printf("N should be less than 10\n");
			return -1;
		}
	} else {
		printf("pass N as argument\n");
		return -1;
	}
	
	int fd[10][2];
	int i=0;
	pid_t pid;
	
	//generate N child and pipes
	for (i; i<N;i++){
		pipe(fd[i]);
		//child
		if((pid=fork())==0){
			break;
		}
	}
	
	//child
	if(pid==0){
		float temp;
		temp=intpow(EUL,i+1)/((float)(i+1));
		write(fd[i][WRITE], &temp, sizeof(float));
		close(fd[i][WRITE]);
		close(fd[i][READ]);	
	//father	
	} else {
		float sum=0,temp=0;
		for(int i = 0; i<N; i++){
			read(fd[i][READ], &temp, sizeof(float));
			sum+=temp;	
			close(fd[i][WRITE]);
			close(fd[i][READ]);	
		}
		printf("Sum: %g\n", sum);
	}

	return 0;
}	
