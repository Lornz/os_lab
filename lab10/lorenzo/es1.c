#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define THRDNUM 3
pthread_cond_t cond[THRDNUM];
pthread_mutex_t mutex;


void *wait_fnct(void* idptr){
	int i= (int) idptr;
	pthread_cond_wait(&cond[i],&mutex);
}

int main(int argc, char *argv[]){

	//allocate array of id
	pthread_t thr_id[THRDNUM];
	char ended[THRDNUM];
	pthread_mutex_init(&mutex,NULL);	
	//create threads
	for(int i=0;i<THRDNUM;i++){
		pthread_cond_init(&cond[i],NULL);
		int retval=pthread_create(&thr_id[i],NULL, wait_fnct, (void *) i);
		if(retval)
			printf("Error creating threads\n");
	}

	int running=3;
	//create buffer
	char buffer[255];
	pthread_t exitid;
	int found=-1;
	while(running){
		printf("List of threads\n");
		for(int i=0; i<THRDNUM;i++){
			if(!ended[i])
				printf("Thread:\t%lu\n",thr_id[i]);
		}
		scanf("%s",buffer);
		exitid=atoi(buffer);
	
		found=-1;
		for(int i=0;i<THRDNUM;i++){
			if(exitid==thr_id[i]){
				found=i;
				pthread_cond_signal(&cond[i]);
			}
		}
		if(found==-1){
			printf("not found\n");
			continue;
		}
		pthread_join(thr_id[found],NULL);
		ended[found]=1;
		running--;
		}
	
	//join threads
	
	printf("Father exited\n");
	
	return 0;
}
